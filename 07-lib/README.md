# Linux 上的 C 語言的套件

* posix -- 預設
* glib -- (常用) https://docs.gtk.org/glib/#structs
* gsl -- (科學計算) https://www.math.utah.edu/software/gsl/gsl-design_toc.html#SEC9
* sdl -- (繪圖板) https://www.libsdl.org/
* gtk -- (視窗) https://docs.gtk.org/gtk4/#classes
* libpcap -- https://www.tcpdump.org/index.html#latest-releases
* sqlite -- https://www.sqlite.org/docs.html
* libpq -- https://www.postgresql.org/docs/9.1/libpq.html
* libxml2 -- http://xmlsoft.org/
* json-c -- https://github.com/json-c/json-c
* libcurl -- https://curl.se/libcurl/c/ 

## 發布

* https://www.debian.org/doc/manuals/debian-reference/index.en.html
* https://linuxconfig.org/c-development-on-linux-introduction-i
    1. C development on Linux – Introduction
    2. Comparison between C and other programming languages
    3. Types, operators, variables
    4. Flow control
    5. Functions
    6. Pointers and arrays
    7. Structures
    8. Basic I/O
    9. Coding style and recommendations
    10. [Building a program](https://linuxconfig.org/c-development-on-linux-building-a-program-x)
    11. [Packaging for Debian and Fedora](https://linuxconfig.org/c-development-on-linux-packaging-for-debian-and-fedora-xi)
    12. [Getting a package in the official Debian repositories](https://linuxconfig.org/c-development-on-linux-getting-a-package-in-the-official-debian-repositories-xii)